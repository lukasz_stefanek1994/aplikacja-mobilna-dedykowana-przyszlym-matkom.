package lukasz.stefanek.ciaza;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import lukasz.stefanek.ciaza.names.RandomChildNameActivity;
import lukasz.stefanek.ciaza.visit.NotificationListActivity;

import static lukasz.stefanek.ciaza.R.id.progressBar;

public class MomMenuActivity extends AppCompatActivity {

    public static final int MONTHS_PREGNANCY = 40;
    private ImageView childPhoto;
    private String birthText;
    private TextView description;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mom_menu);
        setTitle("Aktualny stan Twojej ciąży");

        birthText = getSharedPreferences("dane", 0).getString("birthText",null);
        int whichWeek = Integer.parseInt(birthText);
        childPhoto = (ImageView) findViewById(R.id.sizeChildPhoto);
        childPhoto.setImageResource(images[whichWeek-1]);

        description = (TextView)findViewById(R.id.descEveryWeek);
        description.setText(DescriptionEveryWeek.description[whichWeek-1]);

        Intent intentName = getIntent();    //pobieramy intencje
        String name = intentName.getStringExtra("momName");//a z niej pobieramy tekst
        TextView messageName = (TextView)findViewById(R.id.messageName);    //pobieramy wnetrze TextView zeby tam ustawic tekst
        messageName.setText("Witaj " +name+"!");

        Intent intentPhoto = getIntent();
        Bitmap bitmap = (Bitmap) intentPhoto.getParcelableExtra("imagebitmap");
        ImageView imageView = (ImageView)findViewById(R.id.momPhoto);
        imageView.setImageBitmap(bitmap);

        Intent intentDate = getIntent();
        String date = intentDate.getStringExtra("momDate");
        TextView messageDate = (TextView)findViewById(R.id.messageDate);
        messageDate.setText("Obecnie jesteś w "+date+" tygodniu ciąży.");

        ProgressBar mProgress = (ProgressBar) findViewById(progressBar);
        mProgress.setMax(MONTHS_PREGNANCY);
        int data = Integer.parseInt(date);      //tydzien ciazy
        mProgress.setProgress(data);

        TextView daysRemain = (TextView)findViewById(R.id.days_remain);
        daysRemain.setText("pozostało " +(MONTHS_PREGNANCY-data)*7 +" dni");

    }
    public void onWeight(View view)
    {
        Intent intent = new Intent(this,WeightMomActivity.class);
        startActivity(intent);
    }

    public void onVisits(View view)
    {
        Intent intent = new Intent(this,NotificationListActivity.class);
        startActivity(intent);
    }
    public void onRandName(View view)
    {
        Intent intent = new Intent(this,RandomChildNameActivity.class);
        startActivity(intent);
    }

    public void onDevelopment(View view)
    {
        Intent intent = new Intent(this,DevelopmentActivity.class);
        startActivity(intent);
    }

    public static final Integer[] images = {
            R.drawable.tydzien1, R.drawable.tydzien2,R.drawable.tydzien3, R.drawable.tydzien4,
            R.drawable.tydzien5, R.drawable.tydzien6, R.drawable.tydzien7, R.drawable.tydzien8,
            R.drawable.tydzien9, R.drawable.tydzien10, R.drawable.tydzien11, R.drawable.tydzien12,
            R.drawable.tydzien13, R.drawable.tydzien14, R.drawable.tydzien15, R.drawable.tydzien16,
            R.drawable.tydzien17, R.drawable.tydzien18, R.drawable.tydzien19, R.drawable.tydzien20,
            R.drawable.tydzien21, R.drawable.tydzien22, R.drawable.tydzien23, R.drawable.tydzien24,
            R.drawable.tydzien25, R.drawable.tydzien26, R.drawable.tydzien27, R.drawable.tydzien28,
            R.drawable.tydzien29, R.drawable.tydzien30, R.drawable.tydzien31, R.drawable.tydzien32,
            R.drawable.tydzien33, R.drawable.tydzien34, R.drawable.tydzien35, R.drawable.tydzien36,
            R.drawable.tydzien37, R.drawable.tydzien38, R.drawable.tydzien39,R.drawable.tydzien40 };

}
