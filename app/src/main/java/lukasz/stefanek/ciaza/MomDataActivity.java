package lukasz.stefanek.ciaza;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MomDataActivity extends AppCompatActivity {

    public SharedPreferences sharedPreferences;
    private String nameText;
    private String birthText;

    private ImageView momPhoto;
    private EditText momName;
    private EditText birthDate;

    private Bitmap bitmap;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mom_data);
        setTitle("Wypełnij poniższe pola");



        momName = (EditText)findViewById(R.id.mom_name);
        nameText = getSharedPreferences("dane", 0).getString("nameText", null);
        momName.setText(nameText);

        birthDate = (EditText)findViewById(R.id.childbirth_date);
        birthText = getSharedPreferences("dane", 0).getString("birthText",null); ///
        birthDate.setText(birthText);

        momPhoto = (ImageView)findViewById(R.id.mom_photo);
        birthDate = (EditText) findViewById(R.id.childbirth_date);
    }

    public void onAddPhoto(View view)
    {
    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        //File pictureDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        //String pictureName = getPictureName();
        //File imageFile = new File(pictureDirectory,pictureName);
        //Uri pictureUri = Uri.fromFile(imageFile);

        //cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,pictureUri);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,0);
        startActivityForResult(cameraIntent,0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
       bitmap = (Bitmap) data.getExtras().get("data");
        momPhoto.setImageBitmap(bitmap);
    }

    public void onMomMenu(View view) {
        nameText = momName.getText().toString();
        birthText = birthDate.getText().toString();
        sharedPreferences = getSharedPreferences("dane", 0);
        SharedPreferences.Editor edytor = sharedPreferences.edit();
        edytor.putString("nameText", nameText);
        edytor.putString("birthText",birthText);
        edytor.apply();

        Intent intent = new Intent(this, MomMenuActivity.class);
        intent.putExtra("momDate", birthText);
        intent.putExtra("momName", nameText);
        intent.putExtra("imagebitmap",bitmap);


        startActivity(intent);

    }

    /*public String getPictureName() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String timestamp = sdf.format(new Date());
        return "MomPhoto" + timestamp + ".jpg";

    }*/
}





