package lukasz.stefanek.ciaza;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;

public class WeightMomActivity extends AppCompatActivity {

    DatabaseWeightHelper mDatabaseWeightHelper;
    private Button addWeight;
    private  Button showWeightList;
    private  EditText giveWeightt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weight_mom);
        setTitle("Twoja waga");

        addWeight = (Button)findViewById(R.id.btnAddWeight);
        giveWeightt = (EditText) findViewById(R.id.giveWeight);
        showWeightList = (Button)findViewById(R.id.btnWeightList);
        mDatabaseWeightHelper = new DatabaseWeightHelper(this);
        addWeight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Integer newWeight = Integer.parseInt(giveWeightt.getText().toString());
                Float newWeight = Float.parseFloat(giveWeightt.getText().toString());
                String newWeek1 = getSharedPreferences("dane", 0).getString("birthText",null); //zczytanie z SharedPreferences
                String newWeek = newWeek1 +"tyg";
                Date currentDate = new Date();
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                String newDate = dateFormat.format(currentDate);

                if(giveWeightt.length()!=0)
                {
                    AddData(newWeight, newDate, newWeek);
                    //AddData(newWeight);
                    View view = getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                    giveWeightt.setText("");
                }
                else
                {
                    Toast.makeText(WeightMomActivity.this,"Wstaw cos w pole wagi",Toast.LENGTH_SHORT).show();
                }
            }
        });

        showWeightList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WeightMomActivity.this,WeightListActivity.class);
                startActivity(intent);
            }
        });
    }
    private void AddData(Float newWeight, String newDate, String newWeek){
    //private void AddData(String newWeight) {
        boolean insertData = mDatabaseWeightHelper.addData(newWeight,newDate,newWeek);
        //boolean insertData = mDatabaseWeightHelper.addData(newWeight);

        if(insertData)
        {
            Toast.makeText(WeightMomActivity.this,"Dodano!",Toast.LENGTH_SHORT).show();
        }
        else
        {
            Toast.makeText(WeightMomActivity.this,"Nie dodano!",Toast.LENGTH_SHORT).show();
        }

    }



}
