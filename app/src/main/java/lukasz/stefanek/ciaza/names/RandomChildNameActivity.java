package lukasz.stefanek.ciaza.names;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.Random;

import lukasz.stefanek.ciaza.R;

public class RandomChildNameActivity extends AppCompatActivity {

    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private ShakeDetector mShakeDetector;

    Vibrator vibrator;
    TextView wasDrawn;
    TextView shakeInfo;
    TextView editGirlName;
    TextView editBoyName;

    String [] boyNames = new String[]{"Adam", "Marcin", "Wojtek", "Rafał", "Adolf","Hieronim"};
    String [] girlNames = new String[]{"Ania", "Melania", "Pelagia", "Jessica", "Eliza", "Karina"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_random_child_name);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); // block screen orientation
        setTitle("Losowanie imienia dla dziecka");


        shakeInfo = (TextView)findViewById(R.id.shakeInfo);
        wasDrawn = (TextView)findViewById(R.id.wasDrawn);

        // ShakeDetector initialization
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager
                .getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mShakeDetector = new ShakeDetector();
        mShakeDetector.setOnShakeListener(new ShakeDetector.OnShakeListener() {

            @Override
            public void onShake(int count) {
                if(count==3)
                {

                    editGirlName= (TextView)findViewById(R.id.editGirlName);
                    editBoyName= (TextView)findViewById(R.id.editBoyName);

                    Random random = new Random();
                    int sizeTab = boyNames.length;
                    int a = random.nextInt(sizeTab);         //rand from 0 to 3
                    editBoyName.setText(boyNames[a]);
                    editGirlName.setText(girlNames[a]);

                    shakeInfo.setText("Aby wylosować ponownie potrząśnij ");
                    wasDrawn.setText("Wylosowano: ");
                    vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);

                    vibrator.vibrate(600);

                    //handleShakeEvent(count);
                }
                else if(count==2)
                {
                    shakeInfo.setText("Potrzasnij jeszcze jeden raz: ");
                }

                else if(count==1)
                {
                    wasDrawn.setText(" ");
                    shakeInfo.setText("Potrzasnij jeszcze dwa razy: ");
                }

            }
        });


    }

    public void onBtnRandName(View view)
    {

        editGirlName= (TextView)findViewById(R.id.editGirlName);
        editBoyName= (TextView)findViewById(R.id.editBoyName);

        Random random = new Random();
        int sizeTab = boyNames.length;
        int a = random.nextInt(sizeTab);         //rand from 0 to 3
        editBoyName.setText(boyNames[a]);
        editGirlName.setText(girlNames[a]);

    }
    @Override
    public void onResume() {
        super.onResume();
        // Add the following line to register the Session Manager Listener onResume
        mSensorManager.registerListener(mShakeDetector, mAccelerometer,	SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    public void onPause() {
        // Add the following line to unregister the Sensor Manager onPause
        mSensorManager.unregisterListener(mShakeDetector);
        super.onPause();
    }
}
