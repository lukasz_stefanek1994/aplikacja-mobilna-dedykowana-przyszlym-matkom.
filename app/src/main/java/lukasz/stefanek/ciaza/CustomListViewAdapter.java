package lukasz.stefanek.ciaza;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
;

public class CustomListViewAdapter extends ArrayAdapter<RowItem> {

    Context context;

    public CustomListViewAdapter(Context context, int resourceId,
                                 List<RowItem> items) {
        super(context, resourceId, items);
        this.context = context;
    }

    /*private view holder class*/
    private class ViewHolder {
        ImageView imageView;
        TextView txtWeek;
        TextView txtFruitName;
        TextView txtSize;
        TextView txtWeight;

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        RowItem rowItem = getItem(position);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_development, null);
            holder = new ViewHolder();
            holder.txtFruitName = (TextView) convertView.findViewById(R.id.fruitName);
            holder.txtWeek = (TextView) convertView.findViewById(R.id.week);
            holder.imageView = (ImageView) convertView.findViewById(R.id.icon);
            holder.txtSize = (TextView)convertView.findViewById(R.id.size);
            holder.txtWeight = (TextView)convertView.findViewById(R.id.weight);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();

        holder.txtWeek.setText(rowItem.getWeek());
        holder.txtFruitName.setText(rowItem.getFruitName());
        holder.txtSize.setText(rowItem.getSize());
        holder.txtWeight.setText(rowItem.getWeight());
        holder.imageView.setImageResource(rowItem.getImageId());

        return convertView;
    }
}
