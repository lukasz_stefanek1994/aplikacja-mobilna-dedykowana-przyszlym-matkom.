package lukasz.stefanek.ciaza;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;


import java.util.Calendar;

import lukasz.stefanek.ciaza.notification.ScheduleClient;

public class DoctorVisitsActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {

    private ScheduleClient scheduleClient;

     EditText editVisitKind;
     EditText editDoctorName;
     EditText editQuestions;
     EditText editDescription;
     TextView editDate;
     TextView editTime;
     DatePickerDialog.OnDateSetListener mDateSetListener;
     TimePickerDialog.OnTimeSetListener mTimeSetListener;
     Switch switchRemind;
     Button addRemind;

    int setHour=0;
    int setMinutes=0;
    int setYear;
    int setMonth;
    int setDay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_visits);
        setTitle("Wizyta/kontrola lekarska");

        editVisitKind = (EditText) findViewById(R.id.editVisitKind);
        editDoctorName = (EditText) findViewById(R.id.editDoctorName);
        editQuestions = (EditText) findViewById(R.id.editQuestions);
        editDescription = (EditText) findViewById(R.id.editDescription);
        editDate = (TextView) findViewById(R.id.editDate);
        editTime = (TextView) findViewById(R.id.editTime);

        switchRemind = (Switch) findViewById(R.id.switchRemind);
        switchRemind.setOnCheckedChangeListener(this);

        addRemind = (Button) findViewById(R.id.addRemind);

        switchRemind.setOnCheckedChangeListener(this);
        scheduleClient = new ScheduleClient(this);
        scheduleClient.doBindService();

        //method displays a date
        editDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();

                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(DoctorVisitsActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth, mDateSetListener, year, month+1, day);

                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });
        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                Log.d("Data", "onDateSet: mm/dd/yyyy: " + month + "/" + day + "/" + year);

                String date = month+1 + "/" + day + "/" + year;
                setYear = year;
                setMonth = month;
                setDay = day;
                editDate.setText(date);
            }
        };

        //method displays a time
        editTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int hours = cal.get(Calendar.HOUR_OF_DAY);
                int minutes = cal.get(Calendar.MINUTE);

                TimePickerDialog dialog = new TimePickerDialog(DoctorVisitsActivity.this,mTimeSetListener, hours, minutes,true);
                dialog.show();
            }
        });
        mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                Log.d("Time", "onTimeSet: hh:mm: " + hourOfDay + "/" + minute);
                String time;
                setHour = hourOfDay;
                setMinutes = minute;
                if(minute>9 && hourOfDay>9)
                {
                    time = hourOfDay + ":" + minute;
                }
                else if(minute<9 && hourOfDay>9)
                {
                    time = hourOfDay + ":0" + minute;
                }
                else if(minute>9 && hourOfDay<9)
                {
                    time = "0"+ hourOfDay +":" +minute;
                }
                else
                {
                    time = "0"+ hourOfDay +":0" +minute;
                }
                editTime.setText(time);
                }
        };

    }
    public void onAddVisit(View view)
   {

        String typeVisit = editVisitKind.getText().toString();
        String doctorName = editDoctorName.getText().toString();
        String questions = editQuestions.getText().toString();
        String description = editDescription.getText().toString();
        String visitDate = editDate.getText().toString();
        String visitTime = editTime.getText().toString();

        if ( typeVisit.length() != 0 && visitDate.length() != 0 ) {

            Intent newIntent = getIntent();
            newIntent.putExtra("tag_type", typeVisit);
            newIntent.putExtra("tag_name", doctorName);
            newIntent.putExtra("tag_questions", questions);
            newIntent.putExtra("tag_description", description);
            newIntent.putExtra("tag_date", visitDate);
            newIntent.putExtra("tag_time", visitTime);

            this.setResult(RESULT_OK, newIntent);

            finish();
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked) {
            Toast.makeText(DoctorVisitsActivity.this, "Przypomnij dzien przed wizytą!", Toast.LENGTH_SHORT).show();

            Calendar calendar = Calendar.getInstance();
            calendar.set(setYear,setMonth,setDay-1,setHour,setMinutes,5);

            scheduleClient.setAlarmForNotification(calendar);
            Toast.makeText(DoctorVisitsActivity.this, "Ustawiono przypomnienie: "+ (setDay) +"/"+ (setMonth+1) +"/"+ setYear +"  "+setHour+":"+setMinutes,Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    protected void onStop() {
        if(scheduleClient!=null)
            scheduleClient.doUnbindService();
        super.onStop();

    }
}
