package lukasz.stefanek.ciaza;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.FloatMath;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.List;

public class WeightListActivity extends AppCompatActivity {

    private ListView weightList;
    Cursor dataList;
    Cursor dataChartBar;
    private DatabaseWeightHelper mDatabaseWeightHelper;
    ArrayList<Float> listDataWeight;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_weight);
        setTitle("Lista wag");

        mDatabaseWeightHelper = new DatabaseWeightHelper(this);
        weightList = (ListView)findViewById(R.id.weightList);

        populateListView();

        BarChart chart = (BarChart) findViewById(R.id.chart);

        BarData data = new BarData(getXAxisValues(), getDataSet());
        chart.setData(data);
        chart.setDescription("Twoje wagi");
        chart.animateXY(3000,3000);
        chart.invalidate();

    }

    private void populateListView() {
        dataList = mDatabaseWeightHelper.getData();
        ArrayList<String > listData = new ArrayList<>();
        while(dataList.moveToNext())
        {
            listData.add(+dataList.getFloat(1) +"kg  " + dataList.getString(2)+ " "+dataList.getString(3));

        }
        
        ListAdapter adapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,listData);
        weightList.setAdapter(adapter);
    }
    private ArrayList<BarDataSet> getDataSet() {

        dataChartBar = mDatabaseWeightHelper.getData();
System.out.println(dataChartBar);
        ArrayList<Float > listDataWeight = new ArrayList<>();
        while(dataChartBar.moveToNext())
        {
            listDataWeight.add(dataChartBar.getFloat(1));

        }

        ArrayList<BarEntry> valueSet1 = new ArrayList<>();
        ArrayList<BarDataSet> dataSets = null;


    for(int i=0; i<listDataWeight.size();i++) {
        BarEntry v1e1 = new BarEntry(listDataWeight.get(i),i);
        valueSet1.add(v1e1);
    }



        /*ArrayList<BarEntry> valueSet2 = new ArrayList<>();
        BarEntry v2e1 = new BarEntry(150.000f, 0); // Jan
        valueSet2.add(v2e1);
        BarEntry v2e2 = new BarEntry(90.000f, 1); // Feb
        valueSet2.add(v2e2);
        BarEntry v2e3 = new BarEntry(120.000f, 2); // Mar
        valueSet2.add(v2e3);
        BarEntry v2e4 = new BarEntry(60.000f, 3); // Apr
        valueSet2.add(v2e4);
        BarEntry v2e5 = new BarEntry(20.000f, 4); // May
        valueSet2.add(v2e5);
        BarEntry v2e6 = new BarEntry(80.000f, 5); // Jun
        valueSet2.add(v2e6);
        */
        BarDataSet barDataSet1 = new BarDataSet(valueSet1, "waga w kg");

        barDataSet1.setColors(ColorTemplate.COLORFUL_COLORS);
       // BarDataSet barDataSet2 = new BarDataSet(valueSet2, "Brand 2");
       // barDataSet2.setColors(Color.rgb(0, 155, 0));
        dataSets = new ArrayList<>();
        dataSets.add(barDataSet1);
       // dataSets.add(barDataSet2);
        return dataSets;
    }

    private ArrayList<String> getXAxisValues() {

        dataChartBar = mDatabaseWeightHelper.getData();
        ArrayList<String> listDataWeek = new ArrayList<>();
        while(dataChartBar.moveToNext())
        {
            listDataWeek.add(dataChartBar.getString(3));

        }



        //ArrayList<String> xAxis = new ArrayList<>();
        //xAxis.add("JAN");
       // xAxis.add("FEB");
       // xAxis.add("MAR");
        //xAxis.add("APR");
        //xAxis.add("MAY");
       // xAxis.add("JUN");
        return listDataWeek;
    }

    public void onReturnToAddWeight(View view)
    {
        Intent intent = new Intent(WeightListActivity.this,WeightMomActivity.class);
        startActivity(intent);
    }
}
