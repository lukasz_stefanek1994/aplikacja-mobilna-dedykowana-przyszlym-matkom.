package lukasz.stefanek.ciaza;

/**
 * Created by Lukasz on 30.05.2017.
 */

public class DescriptionEveryWeek
{
    public static final String[] description = new String[] {
            "Aby doszło do zapłodnienia do komórki jajowej musi przedostać się jeden z 250milionów plemników.",
            "W jajnikach zaczyna dojrzewać komórka jajowa, która po połączeniu z plemnikiem da nowe życie.",
            "W momencie zapłodnienia ustalana jest nie tylko płeć dziecka, ale i jego charakter, zdolności oraz wygląd. Obecnie dziecko posiada już głowę z maleńkimi oczkami i uszkami oraz zaczątek mózgu i rdzeń kręgowy.",
            "Formują się obecnie trzy grupy komórek, z których jedna przekształci się z układ nerwowy, druga w układ pokarmowy, a trzecia to szkielet, układ moczowo-płciowy, krwionośny i mięśnie.",
            "Pewna bakteria chroniąca Twoje maleństwo przestaje działać. Od tego momentu zdecydowanie odradza się picia alkoholu, czy palenia papierosów, gdyż ma to bezpośredni wpływ na rozwój płodu.",
            "Zaczyna formować się nosek dziecka. Pojawiają się zawiązki rączek i forma nerek. Przede wszystkim jednak zaczyna bić serce.",
            "Dziecko zaczyna się prostować. Formują się palce u rąk, ale póki co są krótkie i grube na końcu. Pod koniec tygodnia wargi dziecka staną się czułe na dotyk.",
            "Powoli zaczynają się formować stopy i kciuki oraz część narządów wewnętrznych. Podczas badania USG możesz już dostrzec swoje dziecko.",
            "Ciało dziecka zaczyna obrastać delikatnym meszkiem, który ma za zadanie go chronić. W jamie ustnej dziecka zaczynają rozwijać się kubki smakowe, a w krtani więzadła głosowe.",
            "W tym tygodniu mózg dziecka tworzy 25000 nowych neuronów w ciągu każdej minuty. Pod koniec 10. Tygodnia dziecko przekształca się z zarodka w płód.",
            "Wszystkie najważniejsze organy Twojego dziecka są formowane. Zaczynają się tworzyć uszy oraz paznokcie dziecka.",
            "Wielkość Twojego dziecka podwoiła się w ciągu ostatnich dwóch tygodni. Teraz dziecko wygląda bardziej „jak człowiek”, oczy zostały przeniesione do przodu.",
            "W tym tygodniu Twoje dziecko może już piąstkować oraz ssać kciuka. Powieki dziecka są ciągle zamknięte i chronią oczy w czasie ich rozwoju. Kości i czaszka dziecka łączą się.",
            "Skóra na palcach Twojego dziecka tworzy unikalne grzbiety i fałdy. Ramiona dziecka stają się proporcjonalne do jego ciała. Obecnie następuje ponadto szybki wzrost mózgu dziecka.",
            "To właśnie od tego tygodnia możesz zacząć czuć poruszające się wewnątrz Ciebie Twoje dziecko. Ponadto zaczynają rosnąć brwi, rzęsy oraz włosy na głowie.",
            "Dziecko zaczyna ćwiczyć wszystkie rodzaje wyrazu twarzy, takie jak mrużenie, ziewanie czy grymasy. Serce dziecka pompuje już około 25 litrów krwi dziennie.",
            "W tym tygodniu dziecko nabiera znacznej masy, która zapewni mu ciepło zaraz po narodzinach. Z tego powodu bardzo istotny jest Twój przyrost wagi w tym tygodniu.",
            "Uszy dziecka są już całkowicie uformowane i skierowane do przodu, tak aby dziecko mogło rzeczywiście usłyszeć to co mówisz. Dziecko ma już 15cm.",
            "Nerki dziecka są już w pełni funkcjonalne i produkują mocz, który zostaje przez nie wydalony wprost do płynu owodniowego.",
            "Z każdym dniem maluch jest coraz ładniejszy i coraz bardziej przypomina noworodka. Podczas kolejnego badania USG z dużym prawdopodobieństwem będzie można określić jego płeć.",
            "Rozwój układu nerwowego dziecka polepsza jego koordynację ruchową. Dziecko jest już wielkości owoca granatu.",
            "Właśnie rozwijają się zmysły dziecka: jego wzrok, słuch, smak oraz dotyk. Dziecko potrafi usłyszeć już pewne rzeczy i odróżnić jasność od ciemności.",
            "Produkcja tłuszczu w tym tygodniu jest trochę wolniejsza niż produkcja skóry, dlatego dziecko wygląda jakby skóry było za dużo, jednak proporcję niedługo się zrównają.",
            "Twarz Twojego maluszka jest już prawie ukształtowana. Dziecko zyskuje dużą świadomość otoczenia. Twój stres w tym tygodniu szczególnie wpływa na dziecko, dlatego postaraj się denerwować jak najmniej.",
            "Dziecko zaczyna otwierać nozdrza, a struny głosowe są już gotowe do działania. Pamiętaj, że dziecko odpoczywa i jest spokojne kiedy jesteś w ruchu. Natomiast kiedy Ty odpoczywasz, ono się odżywia.",
            "W tym tygodniu kubki smakowe dziecka zaczynają się formować. Dziecko zaczyna również delikatnie otwierać swoje oczy. Natomiast jego waga waha się w okolicach kilograma.",
            "Dziecko już bardzo dokładnie jest w stanie wyczuć różnicę w smaku wód płodowych w zależności, od tego, co akurat jadłaś. Niektóre dzieci mają czkawkę po zjedzeniu czegoś pikantnego.",
            "W tym tygodniu dziecko ma niemal całkowicie rozwinięte płuca. To ułatwi mu oddychanie, gdyby doszło do porodu, jednak nie jest jeszcze w pełni gotowe do przyjścia na świat. Dziecko zaczyna również podsłuchiwać o czym mówisz.",
            "Dziecko od teraz w szybkim tempie zaczyna zbliżać się do wielkości jaką osiągnie po porodzie. W przeciągu najbliższych 11 tygodni masa dziecka się podwoi lub nawet potroi.",
            "Mózg dziecka zaczyna się marszczyć. Dzięki temu, że mózg dziecka zaczyna kontrolować temperaturę ciała, meszek lanugo, którym wcześniej pokryte było dziecko, zaczyna zanikać.",
            "We wstępnie ukształtowanym mózgu dziecka zaczynają tworzyć się połączenia neuronowe. Dziecko waży już ponad 1,5kg.",
            "W tym tygodniu dziecko zaczyna gromadzić większe ilości tłuszczu pod skórą, przez co jego skóra staje się coraz mniej przejrzysta. W miarę jak gromadzi się pod nią tkanka tłuszczowa, jego skóra coraz bardziej zaczyna przypominać wygląd Twojej skóry.",
            "Dziecko przyswaja przeciwciała odpornościowe. Od tego momentu zaczyna przybierać na wadze ok. 200-250g. tygodniowo i rosnąć ok. 2cm na tydzień.",
            "Od tego momentu tworzy się system odpornościowy dziecka, który zaczyna przejmować od Ciebie białe krwinki.",
            "Dziecko w tym tygodniu intensywnie wykorzystuje swój nawyk ssania – cały czas ssie swojego kciuka albo nawet całą dłoń. Dziecko pije prawie pół litra wód płodowych dziennie.",
            "Do końca tego tygodnia dziecko powinno obrócić się główką do dołu, ponieważ w następnym będzie już za duże, aby to zrobić.",
            "Od tego tygodnia dziecko nie jest już określane jako wcześniak, w przypadku wystąpienia przedwczesnego porodu. Obecnie zmniejsza się również aktywność dziecka – będzie bardzo spokojne, przez większość czasu będzie spać.",
            "W jelitach dziecka zgromadziła się już smółka ,czyli pierwsza kupka ,którą zrobi tuż po narodzinach. Nie powinno już kopać tak mocno, jak jeszcze kilka tygodni temu, ponieważ nie ma już miejsca na wyprowadzanie ciosów.",
            "Dziecko ma już wyraźnie zaznaczone rysy twarzy, buzia jest okrągła. Paznokcie ma już na tyle długie, że wystają poza brzeg paluszków.",
            "W 40. Tygodniu ciąży dziecko nadal przybiera na wadze, zwiększa się również obwód jego głowy. Dziecko dostało od Ciebie już wszystko, czego będzie potrzebować na początku swojego życia. Mamo! Powodzenia podczas porodu! "};
}
