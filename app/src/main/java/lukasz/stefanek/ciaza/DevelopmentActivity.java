package lukasz.stefanek.ciaza;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;
import android.app.Activity;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;
import lukasz.stefanek.ciaza.R;

public class DevelopmentActivity extends Activity implements
        OnItemClickListener {

    public static final String[] week = new String[] {
            "Tydzień 1", "Tydzień 2", "Tydzień 3", "Tydzień 4",
            "Tydzień 5", "Tydzień 6", "Tydzień 7", "Tydzień 8",
            "Tydzień 9", "Tydzień 10", "Tydzień 11","Tydzień 12",
            "Tydzień 13", "Tydzień 14", "Tydzień 15","Tydzień 16",
            "Tydzień 17", "Tydzień 18", "Tydzień 19","Tydzień 20",
            "Tydzień 21", "Tydzień 22", "Tydzień 23","Tydzień 24",
            "Tydzień 25", "Tydzień 26", "Tydzień 27", "Tydzień 28",
            "Tydzień 29", "Tydzień 30", "Tydzień 31", "Tydzień 32",
            "Tydzień 33", "Tydzień 34", "Tydzień 35", "Tydzień 36",
            "Tydzień 37", "Tydzień 38", "Tydzień 39", "Tydzień 40"};

    public static final String[] fruitName = new String[] {
            "-", "-", "-", "ziarnko maku",
            "pestka jabłka", "ziarnko groszku", "czarna porzeczka", "malina",
            "oliwka", "suszona morela", "limonka","śliwka",
            "brzoskwinia", "cytryna", "pomarańcza","awokado",
            "cebula", "ziemniak", "mango","banan",
            "owoc granatu", "owoc papai", "grejfrut","melon",
            "główka kalafiora", "główka sałaty", "brukiew", "bakłażan",
            "mała dynia", "duży ogórek", "ananas", "dynia",
            "główka kapusty", "duża dynia", "kokos", "melon",
            "duży melon", "dynia zwyczajna", "arbuz", "arbuz"};

    public static final String[] size = new String[] {
            "długość: -", "długość: -", "długość: -", "długość: 0.36-1.0mm",
            "długość: 2-3mm.", "długość: 6mm.", "długość: 8-10mm.", "długość: 11-15mm.",
            "długość: 23mm.", "długość: 3-4cm.", "długość: 4-6cm.","długość: 5-7cm.",
            "długość: 7-8cm.", "długość: 9cm.", "długość: 10cm.","długość: 11-12cm.",
            "długość: 13cm.", "długość: 14cm.", "długość: 15cm.","długość: 16-17cm.",
            "długość: 18-25cm.", "długość: 26cm", "długość: 30cm","długość: 30cm",
            "długość: 34cm", "długość: 35-37cm", "długość: 37cm", "długość: 38cm",
            "długość: 39cm", "długość: 40cm", "długość: 40cm", "długość: 42cm",
            "długość: 44cm", "długość: 45cm", "długość: 45cm", "długość: 47cm",
            "długość: 50cm", "długość: 53cm", "długość: 54-55cm", "długość: 54-55cm"};

    public static final String[] weight = new String[] {
            "waga: -", "waga: -", "waga: -", "waga: -",
            "waga: -", "waga: -", "waga: -", "waga: 1,1g.",
            "waga: 2g.", "waga: 4g.", "waga: 7-8g.","waga: 13-14g.",
            "waga: 20-23g.", "waga: 40g.", "waga: 60-70g.","waga: 100g.",
            "waga: 120-160g.", "waga: 150-190g.", "waga: 240g.","waga: 260-300g",
            "waga: 360g.", "waga: 400g.", "waga: 400-500g.","waga: 550g.",
            "waga: 680g.", "waga: 700-900g.", "waga: 1kg.", "waga: 1,1kg.",
            "waga: 1,3kg.", "waga: 1,3kg.", "waga: 1,6kg.", "waga: 1,7kg.",
            "waga: 2kg.", "waga: 2,3kg.", "waga: 2,5kg.", "waga: 2,8kg.",
            "waga: 3-3,5kg.", "waga: 3,5-4kg.", "waga: 4-4,2kg.", "waga: 4-4,2kg."};


    public static final Integer[] images = {
            R.drawable.tydzien1, R.drawable.tydzien2,R.drawable.tydzien3, R.drawable.tydzien4,
            R.drawable.tydzien5, R.drawable.tydzien6, R.drawable.tydzien7, R.drawable.tydzien8,
            R.drawable.tydzien9, R.drawable.tydzien10, R.drawable.tydzien11, R.drawable.tydzien12,
            R.drawable.tydzien13, R.drawable.tydzien14, R.drawable.tydzien15, R.drawable.tydzien16,
            R.drawable.tydzien17, R.drawable.tydzien18, R.drawable.tydzien19, R.drawable.tydzien20,
            R.drawable.tydzien21, R.drawable.tydzien22, R.drawable.tydzien23, R.drawable.tydzien24,
            R.drawable.tydzien25, R.drawable.tydzien26, R.drawable.tydzien27, R.drawable.tydzien28,
            R.drawable.tydzien29, R.drawable.tydzien30, R.drawable.tydzien31, R.drawable.tydzien32,
            R.drawable.tydzien33, R.drawable.tydzien34, R.drawable.tydzien35, R.drawable.tydzien36,
            R.drawable.tydzien37, R.drawable.tydzien38, R.drawable.tydzien39,R.drawable.tydzien40 };

    ListView listView;
    List<RowItem> rowItems;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_development);
        setTitle("Rozwój");
        rowItems = new ArrayList<RowItem>();
        for (int i = 0; i < week.length; i++) {
            RowItem item = new RowItem(images[i], week[i], fruitName[i], size[i], weight[i]);
            rowItems.add(item);
        }

        listView = (ListView) findViewById(R.id.list);
        CustomListViewAdapter adapter = new CustomListViewAdapter(this,
                R.layout.list_development, rowItems);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
        Toast toast = Toast.makeText(getApplicationContext(),
                "Item " + (position + 1) + ": " + rowItems.get(position),
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM|Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }
}