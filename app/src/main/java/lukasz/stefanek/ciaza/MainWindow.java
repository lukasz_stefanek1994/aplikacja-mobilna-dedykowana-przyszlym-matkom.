package lukasz.stefanek.ciaza;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainWindow extends AppCompatActivity {

    boolean isFirstOpen = true;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_window);
        setTitle("Aplikacja dla kobiet w ciąży");
        isFirstOpen = getSharedPreferences("danee", 0).getBoolean("isFirstOpen", true);
    }
    public void onMom(View view)
    {
        Intent intent = new Intent(this,MomDataActivity.class);
        startActivity(intent);
        /*if(isFirstOpen)
        {
            isFirstOpen = false;
            sharedPreferences = getSharedPreferences("danee", 0);
            SharedPreferences.Editor edytor = sharedPreferences.edit();
            edytor.putBoolean("isFirstOpen", isFirstOpen);
            edytor.apply();
            Intent intent = new Intent(this,MomDataActivity.class);
            startActivity(intent);

        }  */
        /*else
        {

            Intent intent = new Intent(this, MomMenuActivity.class);
            startActivity(intent);
        }*/
    }
    
    public void onChild(View view)
    {
        Intent intent = new Intent(this,ChildDataActivity.class);
        startActivity(intent);
    }
}
