package lukasz.stefanek.ciaza.visit;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class VisitDatabaseHelper {

    private static final String TAG = VisitDatabaseHelper.class.getSimpleName();

    private  static  final String DATABASE_NAME = "visits.db";
    private  static  final int DATABASE_VERSION = 1;

    private DatabaseOpenHelper openHelper;
    private SQLiteDatabase database;

    public VisitDatabaseHelper(Context aContext) {

        openHelper = new DatabaseOpenHelper(aContext);
        database = openHelper.getWritableDatabase();
    }

    //public void insertData(String typeVisit, String doctorName, String questions, String description, String visitDate, String visitTime) {
    public void insertData(String typeVisit, String doctorName, String questions, String description, String visitDate, String  visitTime) {
        ContentValues values =  new ContentValues();
        values.put(Visits.VISIT_TYPE,typeVisit);
       values.put(Visits.DOCTOR_NAME,doctorName);
       values.put(Visits.QUESTIONS,questions);
        values.put(Visits.DESCRIPTION,description);
        values.put(Visits.VISIT_DATE,visitDate);
       values.put(Visits.VISIT_TIME,visitTime);
        database.insert(Visits.TABLE_NAME, null, values);
    }

    public Cursor getAllData () {

        String buildSQL = "SELECT * FROM " + Visits.TABLE_NAME;

        Log.d(TAG, "getAllData SQL: " + buildSQL);

        return database.rawQuery(buildSQL, null);
    }


    private class DatabaseOpenHelper extends SQLiteOpenHelper {

        public DatabaseOpenHelper(Context aContext) {
            super(aContext, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE " + Visits.TABLE_NAME + " ("
                    + Visits._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + Visits.VISIT_TYPE + " TEXT , "
                    + Visits.DOCTOR_NAME + " TEXT , "
                    + Visits.QUESTIONS + " TEXT , "
                    + Visits.DESCRIPTION + " TEXT , "
                    + Visits.VISIT_DATE + " TEXT , "
                    + Visits.VISIT_TIME + " TEXT );"
            );
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + Visits.TABLE_NAME);
            onCreate(db);
        }
    }
}

