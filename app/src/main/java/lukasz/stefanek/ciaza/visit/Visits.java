package lukasz.stefanek.ciaza.visit;

import android.provider.BaseColumns;



public interface Visits extends BaseColumns {
    public static final String TABLE_NAME = "visits";

    public static final String VISIT_TYPE = "type";
    public static final String DOCTOR_NAME = "name";
    public static final String QUESTIONS = "questions";
    public static final String DESCRIPTION = "description";
    public static final String VISIT_DATE = "date";
    public static final String VISIT_TIME = "time";

}
