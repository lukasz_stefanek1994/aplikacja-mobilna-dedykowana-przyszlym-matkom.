package lukasz.stefanek.ciaza.visit;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import lukasz.stefanek.ciaza.DoctorVisitsActivity;
import lukasz.stefanek.ciaza.R;

public class NotificationListActivity extends AppCompatActivity {

    private CustomCursorAdapter customAdapter;
    private VisitDatabaseHelper databaseHelper;

    private static final int ENTER_DATA_REQUEST_CODE = 1;
    private ListView listView;
    private static final String TAG = NotificationListActivity.class.getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_list);
        setTitle("Twoje wizyty");
        databaseHelper = new VisitDatabaseHelper(this);
        listView = (ListView) findViewById(R.id.result_visitList);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, "clicked on item: " + position);
            }
        });

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                customAdapter = new CustomCursorAdapter(NotificationListActivity.this, databaseHelper.getAllData());
                listView.setAdapter(customAdapter);
            }
        });
    }
    public void onClickEnterData(View btnAdd) {

        startActivityForResult(new Intent(this, DoctorVisitsActivity.class), ENTER_DATA_REQUEST_CODE);

    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ENTER_DATA_REQUEST_CODE && resultCode == RESULT_OK) {

            databaseHelper.insertData(data.getExtras().getString("tag_type"),
                    data.getExtras().getString("tag_name"),
                    data.getExtras().getString("tag_questions"),
                    data.getExtras().getString("tag_description"),
                    data.getExtras().getString("tag_date"),
                    data.getExtras().getString("tag_time"));

            customAdapter.changeCursor(databaseHelper.getAllData());
        }

    }
}
