package lukasz.stefanek.ciaza.visit;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import lukasz.stefanek.ciaza.R;

/**
 * Created by Lukasz on 21.05.2017.
 */

public class CustomCursorAdapter extends CursorAdapter{
    public CustomCursorAdapter(Context context, Cursor c) {
        super(context, c);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View retView = inflater.inflate(R.layout.visitlist_item, parent, false);
        return retView;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView textViewDateVisit = (TextView) view.findViewById(R.id.text1);
        textViewDateVisit.setText(cursor.getString(cursor.getColumnIndex(cursor.getColumnName(5))));

        TextView textViewTypeVisit = (TextView) view.findViewById(R.id.text2);
        textViewTypeVisit.setText(cursor.getString(cursor.getColumnIndex(cursor.getColumnName(1))));

        TextView textViewDescription = (TextView) view.findViewById(R.id.text3);
        textViewDescription.setText(cursor.getString(cursor.getColumnIndex(cursor.getColumnName(4))));
    }
}
