package lukasz.stefanek.ciaza;

public class RowItem {
    private int imageId;
    private String week;
    private String fruitName;
    private String size;
    private String weight;

    public RowItem(int imageId, String week, String fruitName, String size, String weight) {
        this.imageId = imageId;
        this.week = week;
        this.fruitName = fruitName;
        this.size = size;
        this.weight = weight;
    }
    public int getImageId() {
        return imageId;
    }
    public void setImageId(int imageId) {
        this.imageId = imageId;
    }
    public String getFruitName() {
        return fruitName;
    }
    public void setFruitName(String fruitName) {
        this.fruitName = fruitName;
    }
    public String getSize() {
        return size;
    }
    public void setSize(String size) {
        this.size = size;
    }

    public String getWeek() {
        return week;
    }
    public void setWeek(String week) {
        this.week = week;
    }
    public String getWeight() {
        return weight;
    }
    public void setWeight(String weight) {
        this.weight = weight;
    }


    @Override
    public String toString() {
        return week + "\n" + fruitName + "\n" + size + "\n" + weight;
    }


}